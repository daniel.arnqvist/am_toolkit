use crate::data::{Abilities, Characteristics};
use crate::rules::{score_to_xp, xp_to_score, xp_remaining};
use crate::Character;
use rand::Rng;
use strum::IntoEnumIterator;

// TODO: Make this testable
pub fn generate_characteristics(character: Character) -> Character {
    let mut rng = rand::thread_rng();
    let mut character = character;
    let mut chars_to_decrament = rng.gen_range(0..=3);
    let mut point_pool = 7;

    while chars_to_decrament > 0 {
        let characteristics: Vec<Characteristics> = Characteristics::iter().collect();
        let char_to_reduce = rng.gen_range(0..characteristics.len());
        let xp_current = score_to_xp(
            &(character
                .get_characteristic(&characteristics[char_to_reduce])
                .abs() as u32),
            &1,
        );
        let xp_next = xp_remaining(&xp_current, &1);

        point_pool += xp_next;
        character.set_characteristic(&characteristics[char_to_reduce], -(xp_to_score(&(xp_next + xp_current), &1) as i32),
        );
        chars_to_decrament -= 1;
    }

    while point_pool > 0 {
        let mut characteristics: Vec<Characteristics> = Characteristics::iter().collect();
        let char_to_increase = rng.gen_range(0..characteristics.len());
        let xp_current = score_to_xp(
            &(character
                .get_characteristic(&characteristics[char_to_increase])
                .abs() as u32),
            &1,
        );
        let xp_next = xp_remaining(&xp_current, &1);
        
        if xp_next > point_pool {
            characteristics.remove(char_to_increase);
            continue;
        }

        point_pool -= xp_next;
        character.set_characteristic(
            &characteristics[char_to_increase],
            character.get_characteristic(&characteristics[char_to_increase]) + 1,
        );
    }

    character
}

pub fn early_childhood(character: Character) -> Character {
    let mut rng = rand::thread_rng();
    let mut character = character;
    let mut abilities = vec!(Abilities::AreaLore(String::from("HomeTown")),
                          Abilities::Athletics,
                          Abilities::Awareness,
                          Abilities::Brawl,
                          Abilities::Charm,
                          Abilities::FolkKen,
                          Abilities::Guile,
                          Abilities::Stealth,
                          Abilities::Survival,
                          Abilities::Swim,);
    let mut xp: u32 = 45;

    character.set_ability(&Abilities::Language(String::from("Native")), 75);
    while xp > 0 {
        let ability_to_increase = rng.gen_range(0..abilities.len());
        let xp_to_raise = xp_remaining(character.get_ability(&abilities[ability_to_increase]), &5);
        if xp < xp_to_raise {
            abilities.remove(ability_to_increase);
            continue;
        }
        character.increase_ability(&abilities[ability_to_increase], xp_to_raise);
        xp -= xp_to_raise;
    }

    character.age = 5;
    character
}

// TODO: Add age restrictions. In rules.rs?
