mod creation;
mod dice;

pub use creation::{early_childhood, generate_characteristics};
pub use dice::{DiceRoller, RollResult};

/// Calculate score from xp based on a scaling value.
/// Generally an ability should have a base 5,
/// and an art should have a base 1.

pub fn xp_to_score(xp: &u32, base: &u32) -> u32 {
    if xp == &0 {
        return 0;
    }

    let xp: f64 = *xp as f64;
    let base: f64 = *base as f64;
    let result: f64 = -(1.0 - f64::sqrt(1.0 + 8.0 * (xp / base))) / 2.0;
    return result.floor() as u32;
}

pub fn score_to_xp(score: &u32, base: &u32) -> u32 {
    if score == &0 {
        return 0;
    }
    return (score * (score - 1) / 2 + score) * base;
}

pub fn xp_remaining(xp: &u32, base: &u32) -> u32 {
    let next_score = xp_to_score(xp, base) + 1;
    return score_to_xp(&next_score, base) - xp;
}

#[cfg(test)]
pub mod rules_test {
    use super::*;

    #[test]
    fn xp_to_ability_score_calculates_correctly() {
        assert!(xp_to_score(&5, &5) == 1);
        assert!(xp_to_score(&15, &5) == 2);
        assert!(xp_to_score(&75, &5) == 5);
        assert!(xp_to_score(&275, &5) == 10);
        assert!(xp_to_score(&1050, &5) == 20);
    }

    #[test]
    fn xp_to_art_score_calculates_correctly() {
        assert!(xp_to_score(&1, &1) == 1);
        assert!(xp_to_score(&3, &1) == 2);
        assert!(xp_to_score(&15, &1) == 5);
        assert!(xp_to_score(&55, &1) == 10);
        assert!(xp_to_score(&210, &1) == 20);
    }

    #[test]
    fn score_to_xp_calculates_correctly() {
        assert!(score_to_xp(&1, &5) == 5);
        assert!(score_to_xp(&1, &1) == 1);
        assert!(score_to_xp(&2, &5) == 15);
        assert!(score_to_xp(&2, &1) == 3);
        assert!(score_to_xp(&3, &5) == 30);
        assert!(score_to_xp(&3, &1) == 6);
        assert!(score_to_xp(&20, &5) == 1050);
        assert!(score_to_xp(&20, &1) == 210);
    }

    #[test]
    fn xp_remaining_calculates_correctly() {
        assert!(xp_remaining(&0, &1) == 1);
        assert!(xp_remaining(&3, &1) == 3);
        assert!(xp_remaining(&5, &5) == 10);
        assert!(xp_remaining(&0, &5) == 5);
        assert!(xp_remaining(&20, &5) == 10);
        assert!(xp_remaining(&1000, &5) == 50);
    }
}
