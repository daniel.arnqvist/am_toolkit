use rand::Rng;

#[cfg(test)]
use mockall::{automock, predicate::*};

pub enum RollResult {
    Value(u32),
    Botch(u32),
}

#[cfg_attr(test, automock)]
trait Roll {
    fn d10(&self) -> u32;
}

struct Dice {}
impl Dice {
    pub fn new() -> Self {
        Dice {}
    }
}

impl Roll for Dice {
    fn d10(&self) -> u32 {
        return rand::thread_rng().gen_range(0..10);
    }
}

/// Object used for rolling dice.
pub struct DiceRoller {
    roll: Box<dyn Roll>,
}

impl DiceRoller {
    pub fn new() -> Self {
        Self {
            roll: Box::new(Dice::new()),
        }
    }
    fn new_mocked_dice(dice_mock: Box<dyn Roll>) -> Self {
        Self { roll: dice_mock }
    }

    /// Roll a stress die, returning a RollResult.
    pub fn stress_roll(&self, risk_mod: &u32) -> RollResult {
        match self.roll.d10() {
            0 => return self.handle_botch(risk_mod),
            1 => return self.handle_explode(),
            value => return RollResult::Value(value),
        };
    }

    fn handle_botch(&self, risk_mod: &u32) -> RollResult {
        let mut risk_mod = risk_mod + 1;
        let mut botch_count = 0;
        while risk_mod > 0 {
            let botch_roll_result = self.roll.d10();
            if botch_roll_result == 0 {
                botch_count += 1;
            } else {
                risk_mod -= 1;
            }
        }
        if botch_count != 0 {
            return RollResult::Botch(botch_count);
        }
        return RollResult::Value(0);
    }

    fn handle_explode(&self) -> RollResult {
        let mut roll_multi = 1;
        loop {
            roll_multi *= 2;
            match self.roll.d10() {
                0 => return RollResult::Value(10 * roll_multi),
                1 => {
                    continue;
                }
                value => return RollResult::Value(value * roll_multi),
            };
        }
    }

    /// Roll a simple die, returning a RollResult.
    pub fn simple_roll(&self) -> RollResult {
        return RollResult::Value(self.roll.d10() + 1);
    }
}

#[cfg(test)]
pub mod dice_test {
    use super::*;

    #[test]
    fn stress_die_double_0_is_botch() {
        let mut dice_results = Vec::new();
        dice_results.push(2);
        dice_results.push(0);
        dice_results.push(0);

        let mut mock_dice = MockRoll::new();
        mock_dice
            .expect_d10()
            .times(3)
            .returning(move || dice_results.pop().unwrap());

        let dice_roller = DiceRoller::new_mocked_dice(Box::new(mock_dice));
        let result = dice_roller.stress_roll(&0);
        assert!(matches!(result, RollResult::Botch(1)));
    }

    #[test]
    fn stress_die_roll_of_a_1_doubles_next_roll() {
        let mut dice_results = Vec::new();
        dice_results.push(5);
        dice_results.push(1);

        let mut mock_dice = MockRoll::new();
        mock_dice
            .expect_d10()
            .times(2)
            .returning(move || dice_results.pop().unwrap());

        let mut dice_roller = DiceRoller::new_mocked_dice(Box::new(mock_dice));
        let result = dice_roller.stress_roll(&0);
        assert!(matches!(result, RollResult::Value(10)));
    }

    #[test]
    fn stress_die_roll_of_three_1_octuples_next_roll() {
        let mut dice_results = Vec::new();
        dice_results.push(5);
        dice_results.push(1);
        dice_results.push(1);
        dice_results.push(1);

        let mut mock_dice = MockRoll::new();
        mock_dice
            .expect_d10()
            .times(4)
            .returning(move || dice_results.pop().unwrap());

        let mut dice_roller = DiceRoller::new_mocked_dice(Box::new(mock_dice));
        let result = dice_roller.stress_roll(&0);
        assert!(matches!(result, RollResult::Value(40)));
    }

    #[test]
    fn stress_die_roll_0_counts_as_10_after_1() {
        let mut dice_results = Vec::new();
        dice_results.push(0);
        dice_results.push(1);

        let mut mock_dice = MockRoll::new();
        mock_dice
            .expect_d10()
            .times(2)
            .returning(move || dice_results.pop().unwrap());

        let mut dice_roller = DiceRoller::new_mocked_dice(Box::new(mock_dice));
        let result = dice_roller.stress_roll(&0);
        assert!(matches!(result, RollResult::Value(20)));
    }
}
