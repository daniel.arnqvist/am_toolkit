use crate::data::types::*;
use std::collections::HashMap;
use std::string::ToString;

use serde::{Deserialize, Serialize};

/// A struct representing a character sheet.
#[derive(Serialize, Deserialize, Debug)]
pub struct Character {
    pub age: u32,
    abilities: HashMap<String, u32>,
    arts: HashMap<String, u32>,
    characteristics: HashMap<String, i32>,
}

impl Character {
    pub fn new() -> Self {
        Self {
            age: 0,
            abilities: HashMap::new(),
            arts: HashMap::new(),
            characteristics: HashMap::from([
                (String::from("Intelligence"), 0),
                (String::from("Perception"), 0),
                (String::from("Strength"), 0),
                (String::from("Stamina"), 0),
                (String::from("Presence"), 0),
                (String::from("Communication"), 0),
                (String::from("Dexterity"), 0),
                (String::from("Quickness"), 0),
            ]),
        }
    }

    pub fn get_ability(&self, ability: &Abilities) -> &u32 {
        match &self.abilities.get(&ability.to_string()) {
            Some(value) => *value,
            None => &0,
        }
    }

    pub fn increase_ability(&mut self, ability: &Abilities, value: u32) {
        let current: u32;
        match self.abilities.get(&ability.to_string()) {
            Some(xp) => current = *xp,
            None => current = 0
        }
        self.abilities.insert(ability.to_string(), current + value);
    }

    pub fn set_ability(&mut self, ability: &Abilities, value: u32) {
        self.abilities.insert(ability.to_string(), value);
    }

    pub fn get_art(&self, art: &Arts) -> &u32 {
        match &self.arts.get(&art.to_string()) {
            Some(value) => *value,
            None => &0,
        }
    }

    pub fn set_art(&mut self, art: Arts, value: u32) {
        self.arts.insert(art.to_string(), value);
    }

    pub fn get_characteristic(&self, char: &Characteristics) -> &i32 {
        match &self.characteristics.get(&char.to_string()) {
            Some(value) => *value,
            None => &0,
        }
    }

    pub fn set_characteristic(&mut self, char: &Characteristics, value: i32) {
        self.characteristics.insert(char.to_string(), value);
    }
}
