use serde::{Deserialize, Serialize};
use std::fmt;
use strum_macros::{Display, EnumIter};

// TODO(Daniel): Lägg till en type funktion som returnerar villken typ värdet ska vara under
/// A list of all abilities.
#[derive(Hash, Eq, PartialEq, Serialize, Deserialize, Debug)]
pub enum Abilities {
    AnimalHandling,
    AreaLore(String),
    Athletics,
    Awareness,
    Bargain,
    Brawl,
    Carouse,
    Charm,
    Chiurgy,
    Cncentration,
    Craft(String),
    Etiquette,
    FolkKen,
    Guile,
    Hunt,
    Intrigue,
    Leadership,
    Legerdemain,
    Language(String),
    Music,
    OrganizationLore(String),
    Profession(String),
    Ride,
    Stealth,
    Survival,
    Swim,
    Teaching,
    ArtesLiberales,
    CivilLaw,
    CanonLaw,
    CommonLaw,
    Medicine,
    Philosophiae,
    Theology,
    CodeOfHermes,
    DominionLore,
    FaerieLore,
    Finesse,
    InfernalLore,
    MagicLore,
    MagicTheory,
    ParmaMagica,
    Penetration,
    Bows,
    GreatWeapon,
    SingleWeapon,
    ThrownWeapon,
    AnimalKen,
    Dowsing,
    EnchantingMusic,
    Entrancement,
    MagicSensitivity,
    Premonitions,
    SecondSight,
    SenseHoliness,
    SenseUnholiness,
    Shapeshifter,
    WildernessSense,
}

/// Implementation of fmt::Display to handle abilities with strings in a nice way.
impl fmt::Display for Abilities {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            Abilities::AreaLore(lore) => write!(f, "AreaLore({})", lore),
            Abilities::Craft(craft) => write!(f, "Craft({})", craft),
            Abilities::OrganizationLore(organization) => {
                write!(f, "OrganizationLore({})", organization)
            }
            Abilities::Language(lang) => write!(f, "Language({})", lang),
            Abilities::Profession(profession) => write!(f, "Profession({})", profession),
            _ => write!(f, "{:?}", self),
        }
    }
}

/// A list of all arts.
#[derive(Display, Hash, Eq, PartialEq, Serialize, Deserialize, Debug)]
pub enum Arts {
    Creo,
    Intellego,
    Muto,
    Perdo,
    Rego,
    Animal,
    Aquam,
    Corpus,
    Herbam,
    Ignem,
    Imaginem,
    Mentem,
    Terram,
    Vim,
}

/// A list of all Characteristics.
#[derive(Display, Hash, Eq, PartialEq, Serialize, Deserialize, Debug, EnumIter)]
pub enum Characteristics {
    Intelligence,
    Perception,
    Strength,
    Stamina,
    Presence,
    Communication,
    Dexterity,
    Quickness,
}
