mod data;
mod rules;

use data::Character;
use rules::{early_childhood , generate_characteristics};

use clap::Parser;
use serde_json::Result;

/// Command line toolkit for the Ars Magica 5ed role playing game.
#[derive(Parser, Debug)]
struct Args {
    #[command(subcommand)]
    action: Action,
}

#[derive(clap::Subcommand, Debug)]
enum Action {
    /// Generate new character.
    Generate,
}

fn main() -> Result<()> {
    let args = Args::parse();
    println!("{:?}", args);

    match args.action {
        Action::Generate => generate()?,
    };

    Ok(())
}

fn generate() -> Result<()> {
    let character = Character::new();

    let character = generate_characteristics(character);
    let character = early_childhood(character);

    let json = serde_json::to_string_pretty(&character)?;
    println!("{}", json);

    Ok(())
}
